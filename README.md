# Docker + Consul + Vault

How to use docker-compose to spin up a Vault instance backed by Consul.
brought up todate for vault 1.10

based in this chaps work:

A full description can be found on my website: https://www.marcolancini.it/2017/blog-vault/

![Vault UI](https://www.marcolancini.it/images/posts/blog_vault_2.jpg)


## Usage

#### First Run

1. Start services: `docker-compose up`
2. Init vault:     `./bin/setup.sh`
3. When done:      `docker-compose down`

Data will be persisted in the `_data` folder.


#### Subsequent Runs

1. Start services: `docker-compose up`
2. Unseal vault:   `bin/unseal.sh`


#### Backup

1. Start services: `docker-compose up`
2. Run backup:     `_bin/backup.sh`


#### Remove all data

1. Stop services: `docker-compose down --volumes`
2. Clear persisted data: `_bin/clean.sh`
